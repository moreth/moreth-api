package httpConfiguration

const (
	IsSecured  bool   = false
	Host       string = "localhost"
	Port       int    = 8080
	TokenStep1 string = ""
	TokenStep2 string = ""
	TokenStep3 string = ""
)
