package connection

import (
	"database/sql"
	"fmt"

	_ "github.com/go-sql-driver/mysql"
	dbConfiguration "gitlab.com/moreth/moreth-api/configuration/db"
)

func connectDb() *sql.DB {
	cnnString := fmt.Sprintf("%s:%s@tcp(%s:%d)/%s", dbConfiguration.Login, dbConfiguration.Password, dbConfiguration.Host, dbConfiguration.Port, dbConfiguration.Name)
	db, err := sql.Open("mysql", cnnString)
	if err != nil {
		panic(err.Error())
	}
	return db
}

func SelectQuery(sqlSelect string, args ...interface{}) *sql.Rows {
	db := connectDb()
	defer db.Close()
	results, err := db.Query(sqlSelect, args...)
	if err != nil {
		panic(err.Error())
	}
	return results
}

func InsertQuery(sqlInsert string, args ...interface{}) int64 {
	db := connectDb()
	defer db.Close()
	result, err := db.Exec(sqlInsert, args...)
	if err != nil {
		panic(err.Error())
	} else {
		id, err := result.LastInsertId()
		if err != nil {
			panic(err.Error())
		} else {
			return id
		}
	}
}

func UpdateQuery(sqlUpdate string, args ...interface{}) {
	db := connectDb()
	defer db.Close()
	result, err := db.Exec(sqlUpdate, args...)
	if err != nil {
		panic(err.Error())
	} else {
		_, err := result.LastInsertId()
		if err != nil {
			panic(err.Error())
		}
	}
}

func DeleteQuery(sqlDelete string, args ...interface{}) {
	db := connectDb()
	defer db.Close()
	result, err := db.Exec(sqlDelete, args...)
	if err != nil {
		panic(err.Error())
	} else {
		_, err := result.LastInsertId()
		if err != nil {
			panic(err.Error())
		}
	}
}
