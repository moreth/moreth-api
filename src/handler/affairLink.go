package handler

import (
	"net/http"
)

func AffairLink(writer http.ResponseWriter, request *http.Request) {
	if request.Method == http.MethodGet {
		affairLinkGet(writer, request)
	} else if request.Method == http.MethodPost {
		affairLinkPost(writer, request)
	} else if request.Method == http.MethodPut {
		affairLinkPut(writer, request)
	} else {
		Error(writer, request, http.StatusForbidden, nil)
	}
}

func affairLinkGet(writer http.ResponseWriter, request *http.Request) {
}

func affairLinkPost(writer http.ResponseWriter, request *http.Request) {
}

func affairLinkPut(writer http.ResponseWriter, request *http.Request) {
}
