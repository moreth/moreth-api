package handler

import (
	"net/http"
)

func AffairInfo(writer http.ResponseWriter, request *http.Request) {
	if request.Method == http.MethodGet {
		affairInfoGet(writer, request)
	} else if request.Method == http.MethodPost {
		affairInfoPost(writer, request)
	} else if request.Method == http.MethodPut {
		affairInfoPut(writer, request)
	} else {
		Error(writer, request, http.StatusForbidden, nil)
	}
}

func affairInfoGet(writer http.ResponseWriter, request *http.Request) {
}

func affairInfoPost(writer http.ResponseWriter, request *http.Request) {
}

func affairInfoPut(writer http.ResponseWriter, request *http.Request) {
}
