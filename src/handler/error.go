package handler

import (
	"fmt"
	"net/http"
)

func Error(writer http.ResponseWriter, request *http.Request, code int, err error) {
	if code == 0 {
		code = http.StatusNotFound
	}
	var text string = http.StatusText(code)
	if err != nil {
		text = fmt.Sprint(err)
	}
	http.Error(writer, text, code)
}
