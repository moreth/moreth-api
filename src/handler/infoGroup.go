package handler

import (
	"io"
	"net/http"

	jsonModel "gitlab.com/moreth/moreth-api/model/json"
)

func InfoGroup(writer http.ResponseWriter, request *http.Request) {
	if request.Method == http.MethodGet {
		infoGroupGet(writer, request)
	} else {
		Error(writer, request, http.StatusForbidden, nil)
	}
}

func infoGroupGet(writer http.ResponseWriter, request *http.Request) {
	writer.Header().Set("Content-Type", "application/json")
	var infoGroupValue jsonModel.InfoGroupValues = jsonModel.FillInfoGroupFromDb()
	b, err := infoGroupValue.ToJson()
	if err != nil {
		Error(writer, request, http.StatusInternalServerError, err)
	}
	io.WriteString(writer, string(b))
}
