package handler

import (
	"net/http"
)

func EntityAffair(writer http.ResponseWriter, request *http.Request) {
	if request.Method == http.MethodGet {
		entityAffairGet(writer, request)
	} else if request.Method == http.MethodPost {
		entityAffairPost(writer, request)
	} else if request.Method == http.MethodPut {
		entityAffairPut(writer, request)
	} else {
		Error(writer, request, http.StatusForbidden, nil)
	}
}

func entityAffairGet(writer http.ResponseWriter, request *http.Request) {
}

func entityAffairPost(writer http.ResponseWriter, request *http.Request) {
}

func entityAffairPut(writer http.ResponseWriter, request *http.Request) {
}
