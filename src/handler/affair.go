package handler

import (
	"net/http"
)

func Affair(writer http.ResponseWriter, request *http.Request) {
	if request.Method == http.MethodGet {
		affairGet(writer, request)
	} else if request.Method == http.MethodPost {
		affairPost(writer, request)
	} else if request.Method == http.MethodPut {
		affairPut(writer, request)
	} else {
		Error(writer, request, http.StatusForbidden, nil)
	}
}

func affairGet(writer http.ResponseWriter, request *http.Request) {
}

func affairPost(writer http.ResponseWriter, request *http.Request) {
}

func affairPut(writer http.ResponseWriter, request *http.Request) {
}
