package handler

import (
	"io"
	"net/http"

	"gitlab.com/moreth/moreth-api/model"
	jsonModel "gitlab.com/moreth/moreth-api/model/json"
)

func InfoValue(writer http.ResponseWriter, request *http.Request) {
	if request.Method == http.MethodGet {
		infoValueGet(writer, request)
	} else {
		Error(writer, request, http.StatusForbidden, nil)
	}
}

func infoValueGet(writer http.ResponseWriter, request *http.Request) {
	writer.Header().Set("Content-Type", "application/json")
	cs, _ := request.URL.Query()["criteria"]
	var c string = cs[0]
	criteria, err := model.CriteriaFromJsonString(c)
	if err != nil {
		Error(writer, request, http.StatusInternalServerError, err)
	}

	var searchResults jsonModel.SearchResult = jsonModel.FetchSearchResultFromCriterias(criteria)
	b, err := searchResults.ToJson()
	if err != nil {
		Error(writer, request, http.StatusInternalServerError, err)
	}
	io.WriteString(writer, string(b))
}
