package handler

import (
	"net/http"
)

func Entity(writer http.ResponseWriter, request *http.Request) {
	if request.Method == http.MethodGet {
		entityGet(writer, request)
	} else if request.Method == http.MethodPost {
		entityPost(writer, request)
	} else if request.Method == http.MethodPut {
		entityPut(writer, request)
	} else {
		Error(writer, request, http.StatusForbidden, nil)
	}
}

func entityGet(writer http.ResponseWriter, request *http.Request) {
}

func entityPost(writer http.ResponseWriter, request *http.Request) {
}

func entityPut(writer http.ResponseWriter, request *http.Request) {
}
