package handler

import (
	"net/http"
)

func EntityInfo(writer http.ResponseWriter, request *http.Request) {
	if request.Method == http.MethodGet {
		entityInfoGet(writer, request)
	} else if request.Method == http.MethodPost {
		entityInfoPost(writer, request)
	} else if request.Method == http.MethodPut {
		entityInfoPut(writer, request)
	} else {
		Error(writer, request, http.StatusForbidden, nil)
	}
}

func entityInfoGet(writer http.ResponseWriter, request *http.Request) {
}

func entityInfoPost(writer http.ResponseWriter, request *http.Request) {
}

func entityInfoPut(writer http.ResponseWriter, request *http.Request) {
}
