package handler

import (
	"net/http"
)

func EntityLinkInfo(writer http.ResponseWriter, request *http.Request) {
	if request.Method == http.MethodGet {
		entityLinkInfoGet(writer, request)
	} else if request.Method == http.MethodPost {
		entityLinkInfoPost(writer, request)
	} else if request.Method == http.MethodPut {
		entityLinkInfoPut(writer, request)
	} else {
		Error(writer, request, http.StatusForbidden, nil)
	}
}

func entityLinkInfoGet(writer http.ResponseWriter, request *http.Request) {
}

func entityLinkInfoPost(writer http.ResponseWriter, request *http.Request) {
}

func entityLinkInfoPut(writer http.ResponseWriter, request *http.Request) {
}
