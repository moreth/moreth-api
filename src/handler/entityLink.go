package handler

import (
	"net/http"
)

func EntityLink(writer http.ResponseWriter, request *http.Request) {
	if request.Method == http.MethodGet {
		entityLinkGet(writer, request)
	} else if request.Method == http.MethodPost {
		entityLinkPost(writer, request)
	} else if request.Method == http.MethodPut {
		entityLinkPut(writer, request)
	} else {
		Error(writer, request, http.StatusForbidden, nil)
	}
}

func entityLinkGet(writer http.ResponseWriter, request *http.Request) {
}

func entityLinkPost(writer http.ResponseWriter, request *http.Request) {
}

func entityLinkPut(writer http.ResponseWriter, request *http.Request) {
}
