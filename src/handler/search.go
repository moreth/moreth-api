package handler

import (
	"io"
	"net/http"

	"gitlab.com/moreth/moreth-api/model"
	jsonModel "gitlab.com/moreth/moreth-api/model/json"
)

func Search(writer http.ResponseWriter, request *http.Request) {
	if request.Method == http.MethodGet {
		searchGet(writer, request)
	} else {
		Error(writer, request, http.StatusForbidden, nil)
	}
}

func searchGet(writer http.ResponseWriter, request *http.Request) {
	writer.Header().Set("Content-Type", "application/json")
	cs, _ := request.URL.Query()["criteria"]
	var criterias []model.Criteria
	for _, c := range cs {
		criteria, err := model.CriteriaFromJsonString(c)
		if err != nil {
			Error(writer, request, http.StatusInternalServerError, err)
		}
		criterias = append(criterias, criteria)
	}
	if len(criterias) == 0 {
		criterias = append(criterias, model.Criteria{GroupName: "%", Name: "%"})
	}
	var searchResult jsonModel.SearchResult = jsonModel.FetchSearchResultFromCriterias(criterias...)
	b, err := searchResult.ToJson()
	if err != nil {
		Error(writer, request, http.StatusInternalServerError, err)
	}
	io.WriteString(writer, string(b))
}
