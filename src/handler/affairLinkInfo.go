package handler

import (
	"net/http"
)

func AffairLinkInfo(writer http.ResponseWriter, request *http.Request) {
	if request.Method == http.MethodGet {
		affairLinkInfoGet(writer, request)
	} else if request.Method == http.MethodPost {
		affairLinkInfoPost(writer, request)
	} else if request.Method == http.MethodPut {
		affairLinkInfoPut(writer, request)
	} else {
		Error(writer, request, http.StatusForbidden, nil)
	}
}

func affairLinkInfoGet(writer http.ResponseWriter, request *http.Request) {
}

func affairLinkInfoPost(writer http.ResponseWriter, request *http.Request) {
}

func affairLinkInfoPut(writer http.ResponseWriter, request *http.Request) {
}
