package model

import (
	"encoding/json"
)

type Criteria struct {
	GroupName string
	Name      string
	Value     string
}

func CriteriaFromJsonBytes(bytes []byte) (Criteria, error) {
	var result Criteria
	err := json.Unmarshal(bytes, &result)
	if err != nil {
		result = Criteria{}
	}
	if result.GroupName == "" {
		result.GroupName = "%"
	}
	if result.Name == "" {
		result.Name = "%"
	}
	return result, err
}

func CriteriaFromJsonString(s string) (Criteria, error) {
	return CriteriaFromJsonBytes([]byte(s))
}

func (element Criteria) ToJson() ([]byte, error) {
	return json.Marshal(element)
}
