package jsonModel

import (
	"encoding/json"

	dbModel "gitlab.com/moreth/moreth-api/model/db"
)

type AffairLink struct {
	Id           int64  `json:"id"`
	AffairParent Affair `json:"affairParent"`
	AffairChild  Affair `json:"affairChild"`
}

func FillAffairLinkFromDbAffairLinkId(affairLinkId int64) []AffairLink {
	return FillAffairLinkFromDbAffairLink(dbModel.FetchAffairLinkFromAffairLinkId(affairLinkId))
}

func FillAffairLinkFromDbAffairLink(affairsLink ...dbModel.AffairLink) []AffairLink {
	var results []AffairLink
	for _, item := range affairsLink {
		var result AffairLink = AffairLink{
			Id:           item.Id,
			AffairParent: FillAffairFromDbAffair(dbModel.FetchAffairFromAffairId(item.AffairParentId))[0],
			AffairChild:  FillAffairFromDbAffair(dbModel.FetchAffairFromAffairId(item.AffairChildId))[0],
		}
		results = append(results, result)
	}
	return results
}

func AffairLinkFromJsonBytes(bytes []byte) (AffairLink, error) {
	var result AffairLink
	err := json.Unmarshal(bytes, &result)
	if err != nil {
		result = AffairLink{}
	}
	return result, err
}

func AffairLinkFromJsonString(s string) (AffairLink, error) {
	return AffairLinkFromJsonBytes([]byte(s))
}

func (element AffairLink) ToJson() ([]byte, error) {
	return json.Marshal(element)
}
