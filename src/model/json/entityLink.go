package jsonModel

import (
	"encoding/json"

	dbModel "gitlab.com/moreth/moreth-api/model/db"
)

type EntityLink struct {
	Id           int64  `json:"id"`
	EntityParent Entity `json:"entityParentId"`
	EntityChild  Entity `json:"entityChildId"`
}

func FillEntityLinkFromDbEntityLinkId(entityLinkId int64) []EntityLink {
	return FillEntityLinkFromDbEntityLink(dbModel.FetchEntityLinkFromEntityLinkId(entityLinkId))
}

func FillEntityLinkFromDbEntityLink(entitysLink ...dbModel.EntityLink) []EntityLink {
	var results []EntityLink
	for _, item := range entitysLink {
		var result EntityLink = EntityLink{
			Id:           item.Id,
			EntityParent: FillEntityFromDbEntity(dbModel.FetchEntityFromEntityId(item.EntityParentId))[0],
			EntityChild:  FillEntityFromDbEntity(dbModel.FetchEntityFromEntityId(item.EntityChildId))[0],
		}
		results = append(results, result)
	}
	return results
}

func EntityLinkFromJsonBytes(bytes []byte) (EntityLink, error) {
	var result EntityLink
	err := json.Unmarshal(bytes, &result)
	if err != nil {
		result = EntityLink{}
	}
	return result, err
}

func EntityLinkFromJsonString(s string) (EntityLink, error) {
	return EntityLinkFromJsonBytes([]byte(s))
}

func (element EntityLink) ToJson() ([]byte, error) {
	return json.Marshal(element)
}
