package jsonModel

import (
	"encoding/json"

	dbModel "gitlab.com/moreth/moreth-api/model/db"
)

type InfoGroupValues struct {
	Results []InfoGroup `json:"results"`
}

type InfoGroup struct {
	Id          int64      `json:"id"`
	Name        string     `json:"name"`
	IsMandatory string     `json:"isMandatory"`
	Domain      string     `json:"domain"`
	InfoTypes   []InfoType `json:"infos"`
}

func FillInfoGroupFromDb() InfoGroupValues {
	return InfoGroupValues{Results: FillInfoGroupFromDbInfoGroup(dbModel.FetchInfoGroup()...)}
}

func FillInfoGroupFromDbInfoGroupId(infoGroupId int64) []InfoGroup {
	return FillInfoGroupFromDbInfoGroup(dbModel.FetchInfoGroupFromInfoGroupId(infoGroupId))
}

func FillInfoGroupFromDbInfoGroup(infoGroups ...dbModel.InfoGroup) []InfoGroup {
	var results []InfoGroup
	for _, item := range infoGroups {
		var result InfoGroup = InfoGroup{
			Id:          item.Id,
			Name:        item.Name,
			IsMandatory: item.IsMandatory,
			Domain:      item.Domain,
			InfoTypes:   FillInfoTypeFromDbInfoType(dbModel.FetchInfoTypeFromInfoGroupId(item.Id)...),
		}
		results = append(results, result)
	}
	return results
}

func InfoGroupFromJsonBytes(bytes []byte) (InfoGroup, error) {
	var result InfoGroup
	err := json.Unmarshal(bytes, &result)
	if err != nil {
		result = InfoGroup{}
	}
	return result, err
}

func InfoGroupFromJsonString(s string) (InfoGroup, error) {
	return InfoGroupFromJsonBytes([]byte(s))
}

func (element InfoGroupValues) ToJson() ([]byte, error) {
	return json.Marshal(element)
}

func (element InfoGroup) ToJson() ([]byte, error) {
	return json.Marshal(element)
}
