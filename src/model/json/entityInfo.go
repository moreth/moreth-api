package jsonModel

import (
	"encoding/json"

	dbModel "gitlab.com/moreth/moreth-api/model/db"
)

type EntityInfo struct {
	Id         int64  `json:"id"`
	Group      string `json:"group"`
	GroupName  string `json:"groupName"`
	Name       string `json:"name"`
	DataType   string `json:"dataType"`
	Value      string `json:"value"`
	InfoTypeId int64  `json:"infoTypeId"`
	EntityId   int64  `json:"entityId"`
}

func FillEntityInfoFromDbEntityInfoId(affairInfoId int64) []EntityInfo {
	return FillEntityInfoFromDbEntityInfo(dbModel.FetchEntityInfoFromEntityInfoId(affairInfoId))
}

func FillEntityInfoFromDbEntityInfo(affairsInfo ...dbModel.EntityInfo) []EntityInfo {
	var results []EntityInfo
	for _, item := range affairsInfo {
		var infoType dbModel.InfoType = dbModel.FetchInfoTypeFromInfoTypeId(item.InfoTypeId)
		var infoGroup dbModel.InfoGroup = dbModel.FetchInfoGroupFromInfoGroupId(infoType.InfoGroupId)
		var result EntityInfo = EntityInfo{
			Id:         item.Id,
			Group:      item.Group,
			GroupName:  infoGroup.Name,
			Name:       infoType.Name,
			DataType:   infoType.DataType,
			Value:      item.Value,
			InfoTypeId: item.InfoTypeId,
			EntityId:   item.EntityId,
		}
		results = append(results, result)
	}
	return results
}

func EntityInfoFromJsonBytes(bytes []byte) (EntityInfo, error) {
	var result EntityInfo
	err := json.Unmarshal(bytes, &result)
	if err != nil {
		result = EntityInfo{}
	}
	return result, err
}

func EntityInfoFromJsonString(s string) (EntityInfo, error) {
	return EntityInfoFromJsonBytes([]byte(s))
}

func (element EntityInfo) ToJson() ([]byte, error) {
	return json.Marshal(element)
}
