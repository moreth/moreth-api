package jsonModel

import (
	"encoding/json"

	dbModel "gitlab.com/moreth/moreth-api/model/db"
)

type AffairInfo struct {
	Id         int64  `json:"id"`
	Group      string `json:"group"`
	GroupName  string `json:"groupName"`
	Name       string `json:"name"`
	DataType   string `json:"dataType"`
	Value      string `json:"value"`
	InfoTypeId int64  `json:"infoTypeId"`
	AffairId   int64  `json:"affairId"`
}

func FillAffairInfoFromDbAffairInfoId(affairInfoId int64) []AffairInfo {
	return FillAffairInfoFromDbAffairInfo(dbModel.FetchAffairInfoFromAffairInfoId(affairInfoId))
}

func FillAffairInfoFromDbAffairInfo(affairsInfo ...dbModel.AffairInfo) []AffairInfo {
	var results []AffairInfo
	for _, item := range affairsInfo {
		var infoType dbModel.InfoType = dbModel.FetchInfoTypeFromInfoTypeId(item.InfoTypeId)
		var infoGroup dbModel.InfoGroup = dbModel.FetchInfoGroupFromInfoGroupId(infoType.InfoGroupId)
		var result AffairInfo = AffairInfo{
			Id:         item.Id,
			Group:      item.Group,
			GroupName:  infoGroup.Name,
			Name:       infoType.Name,
			DataType:   infoType.DataType,
			Value:      item.Value,
			InfoTypeId: item.InfoTypeId,
			AffairId:   item.AffairId,
		}
		results = append(results, result)
	}
	return results
}

func AffairInfoFromJsonBytes(bytes []byte) (AffairInfo, error) {
	var result AffairInfo
	err := json.Unmarshal(bytes, &result)
	if err != nil {
		result = AffairInfo{}
	}
	return result, err
}

func AffairInfoFromJsonString(s string) (AffairInfo, error) {
	return AffairInfoFromJsonBytes([]byte(s))
}

func (element AffairInfo) ToJson() ([]byte, error) {
	return json.Marshal(element)
}
