package jsonModel

import (
	"encoding/json"

	dbModel "gitlab.com/moreth/moreth-api/model/db"
)

type AffairLinkInfo struct {
	Id           int64  `json:"id"`
	Group        string `json:"group"`
	GroupName    string `json:"groupName"`
	Name         string `json:"name"`
	DataType     string `json:"dataType"`
	Value        string `json:"value"`
	InfoTypeId   int64  `json:"infoTypeId"`
	AffairLinkId int64  `json:"affairId"`
}

func FillAffairLinkInfoFromDbAffairLinkInfoId(affairLinkInfoId int64) []AffairLinkInfo {
	return FillAffairLinkInfoFromDbAffairLinkInfo(dbModel.FetchAffairLinkInfoFromAffairLinkInfoId(affairLinkInfoId))
}

func FillAffairLinkInfoFromDbAffairLinkInfo(affairsLinkInfo ...dbModel.AffairLinkInfo) []AffairLinkInfo {
	var results []AffairLinkInfo
	for _, item := range affairsLinkInfo {
		var infoType dbModel.InfoType = dbModel.FetchInfoTypeFromInfoTypeId(item.InfoTypeId)
		var infoGroup dbModel.InfoGroup = dbModel.FetchInfoGroupFromInfoGroupId(infoType.InfoGroupId)
		var result AffairLinkInfo = AffairLinkInfo{
			Id:           item.Id,
			Group:        item.Group,
			GroupName:    infoGroup.Name,
			Name:         infoType.Name,
			DataType:     infoType.DataType,
			Value:        item.Value,
			InfoTypeId:   item.InfoTypeId,
			AffairLinkId: item.AffairLinkId,
		}
		results = append(results, result)
	}
	return results
}

func AffairLinkInfoFromJsonBytes(bytes []byte) (AffairLinkInfo, error) {
	var result AffairLinkInfo
	err := json.Unmarshal(bytes, &result)
	if err != nil {
		result = AffairLinkInfo{}
	}
	return result, err
}

func AffairLinkInfoFromJsonString(s string) (AffairLinkInfo, error) {
	return AffairLinkInfoFromJsonBytes([]byte(s))
}

func (element AffairLinkInfo) ToJson() ([]byte, error) {
	return json.Marshal(element)
}
