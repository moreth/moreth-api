package jsonModel

import (
	"encoding/json"

	"gitlab.com/moreth/moreth-api/model"
	dbModel "gitlab.com/moreth/moreth-api/model/db"
)

type SearchResult struct {
	Results []Search `json:"results"`
}

type Search struct {
	InfoDomain    string `json:"infoDomain"`
	Id            int64  `json:"id"`
	InfoGroup     string `json:"infoGroup"`
	InfoGroupName string `json:"infoGroupName"`
	InfoName      string `json:"infoName"`
	DataType      string `json:"dataType"`
	InfoValue     string `json:"infoValue"`
	Url           string `json:"url"`
}

func FetchSearchResultFromCriterias(criterias ...model.Criteria) SearchResult {
	return FillSearchResultFromDbSearch(dbModel.FetchSearchFromCriterias(criterias...)...)
}

func FillSearchResultFromDbSearch(Searchs ...dbModel.Search) SearchResult {
	var result SearchResult
	for _, item := range Searchs {
		var s Search = Search{
			InfoDomain:    item.InfoDomain,
			Id:            item.Id,
			InfoGroup:     item.InfoGroup,
			InfoGroupName: item.InfoGroupName,
			InfoName:      item.InfoName,
			DataType:      item.DataType,
			InfoValue:     item.InfoValue,
			Url:           item.Url,
		}
		result.Results = append(result.Results, s)
	}
	return result
}

func SearchFromJsonBytes(bytes []byte) (Search, error) {
	var result Search
	err := json.Unmarshal(bytes, &result)
	if err != nil {
		result = Search{}
	}
	return result, err
}

func SearchFromJsonString(s string) (Search, error) {
	return SearchFromJsonBytes([]byte(s))
}

func (element SearchResult) ToJson() ([]byte, error) {
	return json.Marshal(element)
}

func (element Search) ToJson() ([]byte, error) {
	return json.Marshal(element)
}
