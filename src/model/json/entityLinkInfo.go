package jsonModel

import (
	"encoding/json"

	dbModel "gitlab.com/moreth/moreth-api/model/db"
)

type EntityLinkInfo struct {
	Id           int64  `json:"id"`
	Group        string `json:"group"`
	GroupName    string `json:"groupName"`
	Name         string `json:"name"`
	DataType     string `json:"dataType"`
	Value        string `json:"value"`
	InfoTypeId   int64  `json:"infoTypeId"`
	EntityLinkId int64  `json:"entityId"`
}

func FillEntityLinkInfoFromDbEntityLinkInfoId(entityLinkInfoId int64) []EntityLinkInfo {
	return FillEntityLinkInfoFromDbEntityLinkInfo(dbModel.FetchEntityLinkInfoFromEntityLinkInfoId(entityLinkInfoId))
}

func FillEntityLinkInfoFromDbEntityLinkInfo(entitysLinkInfo ...dbModel.EntityLinkInfo) []EntityLinkInfo {
	var results []EntityLinkInfo
	for _, item := range entitysLinkInfo {
		var infoType dbModel.InfoType = dbModel.FetchInfoTypeFromInfoTypeId(item.InfoTypeId)
		var infoGroup dbModel.InfoGroup = dbModel.FetchInfoGroupFromInfoGroupId(infoType.InfoGroupId)
		var result EntityLinkInfo = EntityLinkInfo{
			Id:           item.Id,
			Group:        item.Group,
			GroupName:    infoGroup.Name,
			Name:         infoType.Name,
			DataType:     infoType.DataType,
			Value:        item.Value,
			InfoTypeId:   item.InfoTypeId,
			EntityLinkId: item.EntityLinkId,
		}
		results = append(results, result)
	}
	return results
}

func EntityLinkInfoFromJsonBytes(bytes []byte) (EntityLinkInfo, error) {
	var result EntityLinkInfo
	err := json.Unmarshal(bytes, &result)
	if err != nil {
		result = EntityLinkInfo{}
	}
	return result, err
}

func EntityLinkInfoFromJsonString(s string) (EntityLinkInfo, error) {
	return EntityLinkInfoFromJsonBytes([]byte(s))
}

func (element EntityLinkInfo) ToJson() ([]byte, error) {
	return json.Marshal(element)
}
