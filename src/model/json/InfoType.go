package jsonModel

import (
	"encoding/json"

	dbModel "gitlab.com/moreth/moreth-api/model/db"
)

type InfoType struct {
	Id          int64  `json:"id"`
	Name        string `json:"name"`
	DataType    string `json:"dataType"`
	IsMandatory string `json:"isMandatory"`
}

func FillInfoTypeFromDbInfoTypeId(infoGroupId int64) []InfoType {
	return FillInfoTypeFromDbInfoType(dbModel.FetchInfoTypeFromInfoTypeId(infoGroupId))
}

func FillInfoTypeFromDbInfoType(infoGroups ...dbModel.InfoType) []InfoType {
	var results []InfoType
	for _, item := range infoGroups {
		var result InfoType = InfoType{
			Id:          item.Id,
			Name:        item.Name,
			DataType:    item.DataType,
			IsMandatory: item.IsMandatory,
		}
		results = append(results, result)
	}
	return results
}

func InfoTypeFromJsonBytes(bytes []byte) (InfoType, error) {
	var result InfoType
	err := json.Unmarshal(bytes, &result)
	if err != nil {
		result = InfoType{}
	}
	return result, err
}

func InfoTypeFromJsonString(s string) (InfoType, error) {
	return InfoTypeFromJsonBytes([]byte(s))
}

func (element InfoType) ToJson() ([]byte, error) {
	return json.Marshal(element)
}
