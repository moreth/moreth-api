package jsonModel

import (
	"encoding/json"
	"time"

	dbModel "gitlab.com/moreth/moreth-api/model/db"
)

type Entity struct {
	Id               int64        `json:"id"`
	ValidationStatus int64        `json:"validationStatus"`
	CreationDate     time.Time    `json:"creationDate"`
	LastUpDate       time.Time    `json:"lastUpDate"`
	Step1Date        time.Time    `json:"step1Date"`
	Step2Date        time.Time    `json:"step2Date"`
	Step3Date        time.Time    `json:"step3Date"`
	NbView           int64        `json:"nbView"`
	EntityInfos      []EntityInfo `json:"infos"`
}

func FillEntityFromDbEntityId(entityId int64) []Entity {
	return FillEntityFromDbEntity(dbModel.FetchEntityFromEntityId(entityId))
}

func FillEntityFromDbEntity(entities ...dbModel.Entity) []Entity {
	var results []Entity
	for _, item := range entities {
		var result Entity = Entity{
			Id:               item.Id,
			ValidationStatus: item.ValidationStatus,
			CreationDate:     item.CreationDate,
			LastUpDate:       item.LastUpDate,
			Step1Date:        item.Step1Date,
			Step2Date:        item.Step2Date,
			Step3Date:        item.Step3Date,
			NbView:           item.NbView,
			EntityInfos:      FillEntityInfoFromDbEntityInfo(dbModel.FetchEntityInfosFromEntityId(item.Id)...),
		}
		results = append(results, result)
	}
	return results
}

func EntityFromJsonBytes(bytes []byte) (Entity, error) {
	var result Entity
	err := json.Unmarshal(bytes, &result)
	if err != nil {
		result = Entity{}
	}
	return result, err
}

func EntityFromJsonString(s string) (Entity, error) {
	return EntityFromJsonBytes([]byte(s))
}

func (element Entity) ToJson() ([]byte, error) {
	return json.Marshal(element)
}
