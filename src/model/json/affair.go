package jsonModel

import (
	"encoding/json"
	"time"

	dbModel "gitlab.com/moreth/moreth-api/model/db"
)

type Affair struct {
	Id               int64        `json:"id"`
	Name             string       `json:"name"`
	Resume           string       `json:"resume"`
	ValidationStatus int64        `json:"validationStatus"`
	StartDate        time.Time    `json:"startDate"`
	EndDate          time.Time    `json:"endDate"`
	CreationDate     time.Time    `json:"creationDate"`
	LastUpDate       time.Time    `json:"lastUpDate"`
	Step1Date        time.Time    `json:"step1Date"`
	Step2Date        time.Time    `json:"step2Date"`
	Step3Date        time.Time    `json:"step3Date"`
	NbView           int64        `json:"nbView"`
	AffairInfos      []AffairInfo `json:"infos"`
}

func FillAffairFromDbAffairId(affairId int64) []Affair {
	return FillAffairFromDbAffair(dbModel.FetchAffairFromAffairId(affairId))
}

func FillAffairFromDbAffair(affairs ...dbModel.Affair) []Affair {
	var results []Affair
	for _, item := range affairs {
		var result Affair = Affair{
			Id:               item.Id,
			Name:             item.Name,
			Resume:           item.Resume,
			ValidationStatus: item.ValidationStatus,
			StartDate:        item.StartDate,
			EndDate:          item.EndDate,
			CreationDate:     item.CreationDate,
			LastUpDate:       item.LastUpDate,
			Step1Date:        item.Step1Date,
			Step2Date:        item.Step2Date,
			Step3Date:        item.Step3Date,
			NbView:           item.NbView,
			AffairInfos:      FillAffairInfoFromDbAffairInfo(dbModel.FetchAffairInfosFromAffairId(item.Id)...),
		}
		results = append(results, result)
	}
	return results
}

func AffairFromJsonBytes(bytes []byte) (Affair, error) {
	var result Affair
	err := json.Unmarshal(bytes, &result)
	if err != nil {
		result = Affair{}
	}
	return result, err
}

func AffairFromJsonString(s string) (Affair, error) {
	return AffairFromJsonBytes([]byte(s))
}

func (element Affair) ToJson() ([]byte, error) {
	return json.Marshal(element)
}
