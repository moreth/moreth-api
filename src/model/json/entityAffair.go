package jsonModel

import (
	"encoding/json"

	dbModel "gitlab.com/moreth/moreth-api/model/db"
)

type EntityAffair struct {
	Id     int64  `json:"id"`
	Entity Entity `json:"entity"`
	Affair Affair `json:"affair"`
}

func FillEntityAffairFromDbEntityAffairId(entityAffairId int64) []EntityAffair {
	return FillEntityAffairFromDbEntityAffair(dbModel.FetchEntityAffairFromEntityAffairId(entityAffairId))
}

func FillEntityAffairFromDbEntityAffair(affairsLinkInfo ...dbModel.EntityAffair) []EntityAffair {
	var results []EntityAffair
	for _, item := range affairsLinkInfo {
		var result EntityAffair = EntityAffair{
			Id: item.Id,
		}
		results = append(results, result)
	}
	return results
}

func EntityAffairFromJsonBytes(bytes []byte) (EntityAffair, error) {
	var result EntityAffair
	err := json.Unmarshal(bytes, &result)
	if err != nil {
		result = EntityAffair{}
	}
	return result, err
}

func EntityAffairFromJsonString(s string) (EntityAffair, error) {
	return EntityAffairFromJsonBytes([]byte(s))
}

func (element EntityAffair) ToJson() ([]byte, error) {
	return json.Marshal(element)
}
