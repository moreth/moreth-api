package dbModel

import (
	"database/sql"
	"time"

	cnn "gitlab.com/moreth/moreth-api/connection"
)

type EntityAffair struct {
	Id        int64
	StartDate time.Time
	EndDate   time.Time
	EntityId  int64
	AffairId  int64
}

func FetchEntityAffairFromEntityAffairId(entityAffairId int64) EntityAffair {
	sqlQuery := "select ENAF_START_DATE, ENAF_END_DATE, ENTI_ID, AFFA_ID from ENTITY_AFFAIR where ENAF_ID = ?"
	rows := cnn.SelectQuery(sqlQuery, entityAffairId)
	defer rows.Close()
	var entityId, affairId int64
	var startDate, endDate time.Time
	var entityAffair EntityAffair
	err := rows.Scan(&startDate, &endDate, &entityId, &affairId)
	if err != nil {
		if err == sql.ErrNoRows {
			// no such entity affair id
		} else {
			panic(err)
		}
	} else {
		entityAffair = EntityAffair{Id: entityAffairId, StartDate: startDate, EndDate: endDate, EntityId: entityId, AffairId: affairId}
	}
	return entityAffair
}

func FetchEntityAffairFromEntityId(entityId int64) []EntityAffair {
	sqlQuery := "select ENAF_ID, ENAF_START_DATE, ENAF_END_DATE, AFFA_ID from ENTITY_AFFAIR where ENTI_ID = ?"
	rows := cnn.SelectQuery(sqlQuery, entityId)
	defer rows.Close()
	var entityAffairId, affairId int64
	var startDate, endDate time.Time
	var entityAffairs []EntityAffair
	for rows.Next() {
		err := rows.Scan(&entityAffairId, &startDate, &endDate, &affairId)
		if err != nil {
			if err == sql.ErrNoRows {
				// no such entity affair id
			} else {
				panic(err)
			}
		} else {
			entityAffairs = append(entityAffairs, EntityAffair{Id: entityAffairId, StartDate: startDate, EndDate: endDate, EntityId: entityId, AffairId: affairId})
		}
	}
	return entityAffairs
}

func FetchEntityAffairFromAffairId(affairId int64) []EntityAffair {
	sqlQuery := "select ENAF_ID, ENAF_START_DATE, ENAF_END_DATE, ENTI_ID from ENTITY_AFFAIR where AFFA_ID = ?"
	rows := cnn.SelectQuery(sqlQuery, affairId)
	defer rows.Close()
	var entityAffairId, entityId int64
	var startDate, endDate time.Time
	var entityAffairs []EntityAffair
	for rows.Next() {
		err := rows.Scan(&entityAffairId, &startDate, &endDate, &entityId)
		if err != nil {
			if err == sql.ErrNoRows {
				// no such entity affair id
			} else {
				panic(err)
			}
		} else {
			entityAffairs = append(entityAffairs, EntityAffair{Id: entityAffairId, StartDate: startDate, EndDate: endDate, EntityId: entityId, AffairId: affairId})
		}
	}
	return entityAffairs
}

func (entityAffair EntityAffair) Save() EntityAffair {
	if entityAffair.Id == 0 {
		entityAffair = entityAffair.insert()
	} else {
		entityAffair = entityAffair.update()
	}
	return entityAffair
}

func (entityAffair EntityAffair) insert() EntityAffair {
	sqlQuery := "insert into ENTITY_AFFAIR(ENAF_START_DATE, ENAF_END_DATE, ENTI_ID, AFFA_ID) VALUES (?, ?, ?, ?)"
	entityAffair.Id = cnn.InsertQuery(sqlQuery, entityAffair.StartDate, entityAffair.EndDate, entityAffair.EntityId, entityAffair.AffairId)
	return entityAffair
}

func (entityAffair EntityAffair) update() EntityAffair {
	sqlQuery := "update ENTITY_AFFAIR set ENAF_START_DATE = ?, ENAF_END_DATE = ?, ENTI_ID = ?, AFFA_ID = ? where ENLI_ID = ?"
	cnn.UpdateQuery(sqlQuery, entityAffair.StartDate, entityAffair.EndDate, entityAffair.EntityId, entityAffair.AffairId, entityAffair.Id)
	return entityAffair
}
