package dbModel

import (
	"database/sql"

	cnn "gitlab.com/moreth/moreth-api/connection"
	"gitlab.com/moreth/moreth-api/model"
)

type Search struct {
	InfoDomain    string
	Id            int64
	InfoGroup     string
	InfoGroupName string
	InfoName      string
	DataType      string
	InfoValue     string
	Url           string
}

func FetchSearchFromCriterias(criterias ...model.Criteria) []Search {
	sqlQuery := "select INFO_DOMAIN, ID, INFO_GROUP, INFO_GROUP_NAME, INFO_NAME, DATA_TYPE, INFO_VALUE, INFO_URL from V_INFO_SEARCH where INFO_GROUP_NAME LIKE ? AND INFO_NAME LIKE ? AND INFO_VALUE like ? order by LEVENSHTEIN(INFO_VALUE, ?) desc"
	var searchs []Search
	var infoDomain, infoGroup, infoGroupName, inforName, dataType, infoValue, infoUrl string
	var id int64
	for _, criteria := range criterias {
		rows := cnn.SelectQuery(sqlQuery, criteria.GroupName, criteria.Name, "%"+criteria.Value+"%", criteria.Value)
		for rows.Next() {
			err := rows.Scan(&infoDomain, &id, &infoGroup, &infoGroupName, &inforName, &dataType, &infoValue, &infoUrl)
			if err != nil {
				if err == sql.ErrNoRows {
					// no search result
				} else {
					panic(err)
				}
			} else {
				searchs = append(searchs, Search{InfoDomain: infoDomain, Id: id, InfoGroup: infoGroup, InfoGroupName: infoGroupName, InfoName: inforName, DataType: dataType, InfoValue: infoValue, Url: infoUrl})
			}
		}
		rows.Close()
	}
	return searchs
}
