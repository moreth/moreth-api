package dbModel

import (
	"database/sql"

	cnn "gitlab.com/moreth/moreth-api/connection"
)

type AffairLinkInfo struct {
	Id           int64
	Group        string
	Value        string
	InfoTypeId   int64
	AffairLinkId int64
}

func FetchAffairLinkInfoFromAffairLinkInfoId(affairLinkInfoId int64) AffairLinkInfo {
	sqlQuery := "select AFII_GROUP, AFII_VALUE, INTY_ID, AFLI_ID from AFFAIR_LINK_INFO where AFII_ID = ?"
	rows := cnn.SelectQuery(sqlQuery, affairLinkInfoId)
	defer rows.Close()
	var infoTypeId, affairLinkId int64
	var group, value string
	var affairLinkInfo AffairLinkInfo
	err := rows.Scan(&group, &value, &infoTypeId, &affairLinkId)
	if err != nil {
		if err == sql.ErrNoRows {
			// no such affair link info id
		} else {
			panic(err)
		}
	} else {
		affairLinkInfo = AffairLinkInfo{Id: affairLinkInfoId, Group: group, Value: value, InfoTypeId: infoTypeId, AffairLinkId: affairLinkId}
	}
	return affairLinkInfo
}

func FetchAffairLinkInfosFromAffairLinkId(affairLinkId int64) []AffairLinkInfo {
	sqlQuery := "select AFII_ID, AFII_GROUP, AFII_VALUE, INTY_ID from AFFAIR_LINK_INFO where AFLI_ID = ?"
	rows := cnn.SelectQuery(sqlQuery, affairLinkId)
	defer rows.Close()
	var infoTypeId, affairLinkInfoId int64
	var group, value string
	var affairLinkInfos []AffairLinkInfo
	err := rows.Scan(&affairLinkInfoId, &group, &value, &infoTypeId)
	if err != nil {
		if err == sql.ErrNoRows {
			// no such affair link info id
		} else {
			panic(err)
		}
	} else {
		affairLinkInfos = append(affairLinkInfos, AffairLinkInfo{Id: affairLinkInfoId, Group: group, Value: value, InfoTypeId: infoTypeId, AffairLinkId: affairLinkId})
	}
	return affairLinkInfos
}

func (affairLinkInfo AffairLinkInfo) Save() AffairLinkInfo {
	if affairLinkInfo.Id == 0 {
		affairLinkInfo = affairLinkInfo.insert()
	} else {
		affairLinkInfo = affairLinkInfo.update()
	}
	return affairLinkInfo
}

func (affairLinkInfo AffairLinkInfo) insert() AffairLinkInfo {
	sqlQuery := "insert into AFFAIR_LINK_INFO(AFII_GROUP, AFII_VALUE, INTY_ID, AFLI_ID) VALUES (?, ?, ?, ?)"
	affairLinkInfo.Id = cnn.InsertQuery(sqlQuery, affairLinkInfo.Group, affairLinkInfo.Value, affairLinkInfo.InfoTypeId, affairLinkInfo.AffairLinkId)
	return affairLinkInfo
}

func (affairLinkInfo AffairLinkInfo) update() AffairLinkInfo {
	sqlQuery := "update AFFAIR_LINK_INFO set AFII_GROUP = ?, AFII_VALUE = ?, INTY_ID = ?, AFLI_ID = ? where AFII_ID = ?"
	cnn.UpdateQuery(sqlQuery, affairLinkInfo.Group, affairLinkInfo.Value, affairLinkInfo.InfoTypeId, affairLinkInfo.AffairLinkId, affairLinkInfo.Id)
	return affairLinkInfo
}
