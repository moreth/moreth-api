package dbModel

import (
	"database/sql"

	cnn "gitlab.com/moreth/moreth-api/connection"
)

type InfoGroup struct {
	Id          int64
	Name        string
	IsMandatory string
	Domain      string
}

func FetchInfoGroup() []InfoGroup {
	sqlQuery := "select INGR_ID, INGR_NAME, INGR_MANDATORY, INGR_DOMAIN from INFO_GROUP"
	rows := cnn.SelectQuery(sqlQuery)
	defer rows.Close()
	var infoGroupId int64
	var name, isMandatory, domain string
	var infoGroups []InfoGroup
	for rows.Next() {
		err := rows.Scan(&infoGroupId, &name, &isMandatory, &domain)
		if err != nil {
			if err == sql.ErrNoRows {
				// no such info group id
			} else {
				panic(err)
			}
		} else {
			infoGroups = append(infoGroups, InfoGroup{Id: infoGroupId, Name: name})
		}
	}
	return infoGroups
}

func FetchInfoGroupFromInfoGroupId(infoGroupId int64) InfoGroup {
	sqlQuery := "select INGR_NAME, INGR_MANDATORY, INGR_DOMAIN from INFO_GROUP where INGR_ID = ?"
	rows := cnn.SelectQuery(sqlQuery, infoGroupId)
	defer rows.Close()
	var name, isMandatory, domain string
	var infoGroup InfoGroup
	err := rows.Scan(&name, &isMandatory, &domain)
	if err != nil {
		if err == sql.ErrNoRows {
			// no such info group id
		} else {
			panic(err)
		}
	} else {
		infoGroup = InfoGroup{Id: infoGroupId, Name: name}
	}
	return infoGroup
}

func (infoGroup InfoGroup) Save() InfoGroup {
	if infoGroup.Id == 0 {
		infoGroup = infoGroup.insert()
	} else {
		infoGroup = infoGroup.update()
	}
	return infoGroup
}

func (infoGroup InfoGroup) insert() InfoGroup {
	sqlQuery := "insert into INFO_GROUP(INGR_NAME, INGR_DATA_TYPE, INGR_MANDATORY, INGR_DOMAIN) VALUES (?, ?, ?, ?)"
	infoGroup.Id = cnn.InsertQuery(sqlQuery, infoGroup.Name, infoGroup.IsMandatory, infoGroup.Domain)
	return infoGroup
}

func (infoGroup InfoGroup) update() InfoGroup {
	sqlQuery := "update INFO_GROUP set INGR_NAME = ?, INGR_DATA_TYPE = ?, INGR_MANDATORY = ?, INGR_ID = ? where INGR_ID = ?"
	cnn.UpdateQuery(sqlQuery, infoGroup.Name, infoGroup.IsMandatory, infoGroup.Domain, infoGroup.Id)
	return infoGroup
}
