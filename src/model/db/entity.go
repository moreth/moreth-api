package dbModel

import (
	"database/sql"
	"time"

	cnn "gitlab.com/moreth/moreth-api/connection"
)

type Entity struct {
	Id               int64
	ValidationStatus int64
	CreationDate     time.Time
	LastUpDate       time.Time
	Step1Date        time.Time
	Step2Date        time.Time
	Step3Date        time.Time
	NbView           int64
}

func FetchEntityFromEntityId(entityId int64) Entity {
	sqlQuery := "select ENTI_VALIDATION_STATUS, ENTI_CREATION_DATE, ENTI_LAST_UP_DATE, ENTI_STEP1_DATE, ENTI_STEP2_DATE, ENTI_STEP3_DATE, ENTI_NB_VIEW from ENTITY where ENTI_ID = ?"
	rows := cnn.SelectQuery(sqlQuery, entityId)
	defer rows.Close()
	var validationStatus, nbView int64
	var creationDate, lastUpDate, step1Date, step2Date, step3Date time.Time
	var entity Entity
	err := rows.Scan(&validationStatus, &creationDate, &lastUpDate, &step1Date, &step2Date, &step3Date, &nbView)
	if err != nil {
		if err == sql.ErrNoRows {
			// no such entity id
		} else {
			panic(err)
		}
	} else {
		entity = Entity{Id: entityId, ValidationStatus: validationStatus, CreationDate: creationDate, LastUpDate: lastUpDate, Step1Date: step1Date, Step2Date: step2Date, Step3Date: step3Date, NbView: nbView}
	}
	return entity
}

func FetchEntitiesFromAffairId(affairId int64) []Entity {
	sqlQuery := "select ENTI.ENTI_ID, ENTI.ENTI_VALIDATION_STATUS, ENTI.ENTI_CREATION_DATE, ENTI.ENTI_LAST_UP_DATE, ENTI.ENTI_STEP1_DATE, ENTI.ENTI_STEP2_DATE, ENTI.ENTI_STEP3_DATE, ENTI.ENTI_NB_VIEW from ENTITY ENTI where ENAF.AFFA_ID = ?"
	rows := cnn.SelectQuery(sqlQuery, affairId)
	defer rows.Close()
	var entityId, validationStatus, nbView int64
	var creationDate, lastUpDate, step1Date, step2Date, step3Date time.Time
	var entities []Entity
	for rows.Next() {
		err := rows.Scan(&entityId, &validationStatus, &creationDate, &lastUpDate, &step1Date, &step2Date, &step3Date, &nbView)
		if err != nil {
			if err == sql.ErrNoRows {
				// no such entity id
			} else {
				panic(err)
			}
		} else {
			entities = append(entities, Entity{Id: entityId, ValidationStatus: validationStatus, CreationDate: creationDate, LastUpDate: lastUpDate, Step1Date: step1Date, Step2Date: step2Date, Step3Date: step3Date, NbView: nbView})
		}
	}
	return entities
}

func (entity Entity) Save() Entity {
	if entity.Id == 0 {
		entity = entity.insert()
	} else {
		entity = entity.update()
	}
	return entity
}

func (entity Entity) insert() Entity {
	sqlQuery := "insert into ENTITY(ENTI_VALIDATION_STATUS, ENTI_CREATION_DATE, ENTI_LAST_UP_DATE) VALUES (?, ?, ?)"
	entity.CreationDate = time.Now()
	entity.LastUpDate = time.Now()
	entity.Id = cnn.InsertQuery(sqlQuery, entity.ValidationStatus, entity.CreationDate, entity.LastUpDate)
	return entity
}

func (entity Entity) update() Entity {
	sqlQuery := "update ENTITY set ENTI_VALIDATION_STATUS = ?, ENTI_LAST_UP_DATE = ? where ENTI_ID = ?"
	entity.LastUpDate = time.Now()
	cnn.UpdateQuery(sqlQuery, entity.ValidationStatus, entity.LastUpDate, entity.Id)
	return entity
}
