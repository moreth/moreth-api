package dbModel

import (
	"database/sql"

	cnn "gitlab.com/moreth/moreth-api/connection"
)

type EntityLinkInfo struct {
	Id           int64
	Group        string
	Value        string
	InfoTypeId   int64
	EntityLinkId int64
}

func FetchEntityLinkInfoFromEntityLinkInfoId(entityLinkInfoId int64) EntityLinkInfo {
	sqlQuery := "select ENII_GROUP, ENII_VALUE, INTY_ID, ENLI_ID from ENTITY_LINK_INFO where ENII_ID = ?"
	rows := cnn.SelectQuery(sqlQuery, entityLinkInfoId)
	defer rows.Close()
	var infoTypeId, entityLinkId int64
	var group, value string
	var entityLinkInfo EntityLinkInfo
	err := rows.Scan(&group, &value, &infoTypeId, &entityLinkId)
	if err != nil {
		if err == sql.ErrNoRows {
			// no such entity link info id
		} else {
			panic(err)
		}
	} else {
		entityLinkInfo = EntityLinkInfo{Id: entityLinkInfoId, Group: group, Value: value, InfoTypeId: infoTypeId, EntityLinkId: entityLinkId}
	}
	return entityLinkInfo
}

func FetchEntityLinkInfoFromEntityLinkId(entityLinkId int64) []EntityLinkInfo {
	sqlQuery := "select ENII_ID, ENII_GROUP, ENII_VALUE, INTY_ID from ENTITY_LINK_INFO where ENLI_ID = ?"
	rows := cnn.SelectQuery(sqlQuery, entityLinkId)
	defer rows.Close()
	var infoTypeId, entityLinkInfoId int64
	var group, value string
	var entityLinkInfos []EntityLinkInfo
	for rows.Next() {
		err := rows.Scan(&entityLinkInfoId, &group, &value, &infoTypeId)
		if err != nil {
			if err == sql.ErrNoRows {
				// no such entity link info id
			} else {
				panic(err)
			}
		} else {
			entityLinkInfos = append(entityLinkInfos, EntityLinkInfo{Id: entityLinkInfoId, Group: group, Value: value, InfoTypeId: infoTypeId, EntityLinkId: entityLinkId})
		}
	}
	return entityLinkInfos
}

func (entityLinkInfo EntityLinkInfo) Save() EntityLinkInfo {
	if entityLinkInfo.Id == 0 {
		entityLinkInfo = entityLinkInfo.insert()
	} else {
		entityLinkInfo = entityLinkInfo.update()
	}
	return entityLinkInfo
}

func (entityLinkInfo EntityLinkInfo) insert() EntityLinkInfo {
	sqlQuery := "insert into ENTITY_LINK_INFO(ENII_GROUP, ENII_VALUE, INTY_ID, ENLI_ID) VALUES (?, ?, ?, ?)"
	entityLinkInfo.Id = cnn.InsertQuery(sqlQuery, entityLinkInfo.Group, entityLinkInfo.Value, entityLinkInfo.InfoTypeId, entityLinkInfo.EntityLinkId)
	return entityLinkInfo
}

func (entityLinkInfo EntityLinkInfo) update() EntityLinkInfo {
	sqlQuery := "update ENTITY_LINK_INFO set ENII_GROUP = ?, ENII_VALUE = ?, INTY_ID = ?, ENLI_ID = ? where ENII_ID = ?"
	cnn.UpdateQuery(sqlQuery, entityLinkInfo.Group, entityLinkInfo.Value, entityLinkInfo.InfoTypeId, entityLinkInfo.EntityLinkId, entityLinkInfo.Id)
	return entityLinkInfo
}
