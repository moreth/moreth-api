package dbModel

import (
	"database/sql"

	cnn "gitlab.com/moreth/moreth-api/connection"
)

type AffairInfo struct {
	Id         int64
	Group      string
	Value      string
	InfoTypeId int64
	AffairId   int64
}

func FetchAffairInfoFromAffairInfoId(affairInfoId int64) AffairInfo {
	sqlQuery := "select AFIN_GROUP, AFIN_VALUE, INTY_ID, AFFA_ID from AFFAIR_INFO where AFIN_ID = ?"
	rows := cnn.SelectQuery(sqlQuery, affairInfoId)
	defer rows.Close()
	var infoTypeId, affairId int64
	var group, value string
	var affairInfo AffairInfo
	err := rows.Scan(&group, &value, &infoTypeId, &affairId)
	if err != nil {
		if err == sql.ErrNoRows {
			// no such affair info id
		} else {
			panic(err)
		}
	} else {
		affairInfo = AffairInfo{Id: affairInfoId, Group: group, Value: value, InfoTypeId: infoTypeId, AffairId: affairId}
	}
	return affairInfo
}

func FetchAffairInfosFromAffairId(affairId int64) []AffairInfo {
	sqlQuery := "select AFIN_ID, AFIN_GROUP, AFIN_VALUE, INTY_ID from AFFAIR_INFO where AFFA_ID = ?"
	rows := cnn.SelectQuery(sqlQuery, affairId)
	defer rows.Close()
	var affairInfoId, infoTypeId int64
	var group, value string
	var affairInfos []AffairInfo
	for rows.Next() {
		err := rows.Scan(&affairInfoId, &group, &value, &infoTypeId)
		if err != nil {
			if err == sql.ErrNoRows {
				// no such affair info id
			} else {
				panic(err)
			}
		} else {
			affairInfos = append(affairInfos, AffairInfo{Id: affairInfoId, Group: group, Value: value, InfoTypeId: infoTypeId, AffairId: affairId})
		}
	}
	return affairInfos
}

func (affairInfo AffairInfo) Save() AffairInfo {
	if affairInfo.Id == 0 {
		affairInfo = affairInfo.insert()
	} else {
		affairInfo = affairInfo.update()
	}
	return affairInfo
}

func (affairInfo AffairInfo) insert() AffairInfo {
	sqlQuery := "insert into AFFAIR_INFO(AFIN_GROUP, AFIN_VALUE, INTY_ID, AFFA_ID) VALUES (?, ?, ?, ?)"
	affairInfo.Id = cnn.InsertQuery(sqlQuery, affairInfo.Group, affairInfo.Value, affairInfo.InfoTypeId, affairInfo.AffairId)
	return affairInfo
}

func (affairInfo AffairInfo) update() AffairInfo {
	sqlQuery := "update AFFAIR_INFO set AFIN_GROUP = ?, AFIN_VALUE = ?, INTY_ID = ?, AFFA_ID = ? where AFIN_ID = ?"
	cnn.UpdateQuery(sqlQuery, affairInfo.Group, affairInfo.Value, affairInfo.InfoTypeId, affairInfo.AffairId, affairInfo.Id)
	return affairInfo
}
