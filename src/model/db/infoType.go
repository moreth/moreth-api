package dbModel

import (
	"database/sql"

	cnn "gitlab.com/moreth/moreth-api/connection"
)

type InfoType struct {
	Id          int64
	Name        string
	DataType    string
	IsMandatory string
	InfoGroupId int64
}

func FetchInfoTypeFromInfoTypeId(infoTypeId int64) InfoType {
	sqlQuery := "select INTY_NAME, INTY_DATA_TYPE, INTY_MANDATORY, INGR_ID from INFO_TYPE where INTY_ID = ?"
	rows := cnn.SelectQuery(sqlQuery, infoTypeId)
	defer rows.Close()
	var infoGroupId int64
	var name, dataType, isMandatory string
	var infoType InfoType
	err := rows.Scan(&name, &dataType, &isMandatory, &infoGroupId)
	if err != nil {
		if err == sql.ErrNoRows {
			// no such info type id
		} else {
			panic(err)
		}
	} else {
		infoType = InfoType{Id: infoTypeId, Name: name, IsMandatory: isMandatory, InfoGroupId: infoGroupId}
	}
	return infoType
}

func FetchInfoTypeFromInfoGroupId(infoGroupId int64) []InfoType {
	sqlQuery := "select INTY_ID, INTY_NAME, INTY_DATA_TYPE, INTY_MANDATORY from INFO_TYPE where INGR_ID = ?"
	rows := cnn.SelectQuery(sqlQuery, infoGroupId)
	defer rows.Close()
	var infoTypeId int64
	var name, dataType, isMandatory string
	var infoTypes []InfoType
	for rows.Next() {
		err := rows.Scan(&infoTypeId, &name, &dataType, &isMandatory)
		if err != nil {
			if err == sql.ErrNoRows {
				// no such info type id
			} else {
				panic(err)
			}
		} else {
			infoTypes = append(infoTypes, InfoType{Id: infoTypeId, Name: name, IsMandatory: isMandatory, InfoGroupId: infoGroupId})
		}
	}
	return infoTypes
}

func (infoType InfoType) Save() InfoType {
	if infoType.Id == 0 {
		infoType = infoType.insert()
	} else {
		infoType = infoType.update()
	}
	return infoType
}

func (infoType InfoType) insert() InfoType {
	sqlQuery := "insert into INFO_TYPE(INTY_NAME, INTY_DATA_TYPE, INTY_MANDATORY, INGR_ID) VALUES (?, ?, ?, ?)"
	infoType.Id = cnn.InsertQuery(sqlQuery, infoType.Name, infoType.DataType, infoType.IsMandatory, infoType.InfoGroupId)
	return infoType
}

func (infoType InfoType) update() InfoType {
	sqlQuery := "update INFO_TYPE set INTY_NAME = ?, INTY_DATA_TYPE = ?, INTY_MANDATORY = ?, INGR_ID = ? where INTY_ID = ?"
	cnn.UpdateQuery(sqlQuery, infoType.Name, infoType.DataType, infoType.IsMandatory, infoType.InfoGroupId, infoType.Id)
	return infoType
}
