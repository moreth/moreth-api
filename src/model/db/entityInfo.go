package dbModel

import (
	"database/sql"

	cnn "gitlab.com/moreth/moreth-api/connection"
)

type EntityInfo struct {
	Id         int64
	Group      string
	Value      string
	InfoTypeId int64
	EntityId   int64
}

func FetchEntityInfoFromEntityInfoId(entityInfoId int64) EntityInfo {
	sqlQuery := "select ENIN_GROUP, ENIN_VALUE, INTY_ID, ENTI_ID from ENTITY_INFO where ENIN_ID = ?"
	rows := cnn.SelectQuery(sqlQuery, entityInfoId)
	defer rows.Close()
	var infoTypeId, entityId int64
	var group, value string
	var entityInfo EntityInfo
	err := rows.Scan(&group, &value, &infoTypeId, &entityId)
	if err != nil {
		if err == sql.ErrNoRows {
			// no such entity info id
		} else {
			panic(err)
		}
	} else {
		entityInfo = EntityInfo{Id: entityInfoId, Group: group, Value: value, InfoTypeId: infoTypeId, EntityId: entityId}
	}
	return entityInfo
}

func FetchEntityInfosFromEntityId(entityId int64) []EntityInfo {
	sqlQuery := "select ENIN_ID, ENIN_GROUP, ENIN_VALUE, INTY_ID, ENTI_ID from ENTITY_INFO where ENTI_ID = ?"
	rows := cnn.SelectQuery(sqlQuery, entityId)
	defer rows.Close()
	var entityInfoId, infoTypeId int64
	var group, value string
	var entityInfos []EntityInfo
	for rows.Next() {
		err := rows.Scan(&entityInfoId, &group, &value, &infoTypeId, &entityId)
		if err != nil {
			if err == sql.ErrNoRows {
				// no such entity info id
			} else {
				panic(err)
			}
		} else {
			entityInfos = append(entityInfos, EntityInfo{Id: entityInfoId, Group: group, Value: value, InfoTypeId: infoTypeId, EntityId: entityId})
		}
	}
	return entityInfos
}

func (entityInfo EntityInfo) Save() EntityInfo {
	if entityInfo.Id == 0 {
		entityInfo = entityInfo.insert()
	} else {
		entityInfo = entityInfo.update()
	}
	return entityInfo
}

func (entityInfo EntityInfo) insert() EntityInfo {
	sqlQuery := "insert into ENTITY_INFO(ENIN_GROUP, ENIN_VALUE, INTY_ID, ENTI_ID) VALUES (?, ?, ?, ?)"
	entityInfo.Id = cnn.InsertQuery(sqlQuery, entityInfo.Group, entityInfo.Value, entityInfo.InfoTypeId, entityInfo.EntityId)
	return entityInfo
}

func (entityInfo EntityInfo) update() EntityInfo {
	sqlQuery := "update ENTITY_INFO set ENIN_GROUP = ?, ENIN_VALUE = ?, INTY_ID = ?, ENTI_ID = ? where ENIN_ID = ?"
	cnn.UpdateQuery(sqlQuery, entityInfo.Group, entityInfo.Value, entityInfo.InfoTypeId, entityInfo.EntityId, entityInfo.Id)
	return entityInfo
}
