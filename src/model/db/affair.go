package dbModel

import (
	"database/sql"
	"time"

	cnn "gitlab.com/moreth/moreth-api/connection"
)

type Affair struct {
	Id               int64
	Name             string
	Resume           string
	ValidationStatus int64
	StartDate        time.Time
	EndDate          time.Time
	CreationDate     time.Time
	LastUpDate       time.Time
	Step1Date        time.Time
	Step2Date        time.Time
	Step3Date        time.Time
	NbView           int64
}

func FetchAffairFromAffairId(affairId int64) Affair {
	sqlQuery := "select AFFA_NAME, AFFA_RESUME, AFFA_VALIDATION_STATUS, AFFA_START_DATE, AFFA_END_DATE, AFFA_CREATION_DATE, AFFA_LAST_UP_DATE, AFFA_STEP1_DATE, AFFA_STEP2_DATE, AFFA_STEP3_DATE, AFFA_NB_VIEW from AFFAIR where AFFA_ID = ?"
	rows := cnn.SelectQuery(sqlQuery, affairId)
	defer rows.Close()
	var validationStatus, nbView int64
	var creationDate, lastUpDate, step1Date, step2Date, step3Date time.Time
	var name, resume string
	var affair Affair
	err := rows.Scan(&name, &resume, &validationStatus, &creationDate, &lastUpDate, &step1Date, &step2Date, &step3Date, &nbView)
	if err != nil {
		if err == sql.ErrNoRows {
			// no such affair id
		} else {
			panic(err)
		}
	} else {
		affair = Affair{Id: affairId, Name: name, Resume: resume, ValidationStatus: validationStatus, CreationDate: creationDate, LastUpDate: lastUpDate, Step1Date: step1Date, Step2Date: step2Date, Step3Date: step3Date, NbView: nbView}
	}
	return affair
}

func FetchAffairsFromEntityId(entityId int64) []Affair {
	sqlQuery := "select AFFA.AFFA_ID, AFFA.AFFA_NAME, AFFA.AFFA_RESUME, AFFA.AFFA_VALIDATION_STATUS, AFFA.AFFA_START_DATE, AFFA.AFFA_END_DATE, AFFA.AFFA_CREATION_DATE, AFFA.AFFA_LAST_UP_DATE, AFFA.AFFA_STEP1_DATE, AFFA.AFFA_STEP2_DATE, AFFA.AFFA_STEP3_DATE, AFFA.AFFA_NB_VIEW from AFFAIR AFFA INNER JOIN ENTITY_AFFAIR ENAF ON AFFA.AFFA_ID = ENAF.AFFA_ID where ENAF.ENTI_ID = ?"
	rows := cnn.SelectQuery(sqlQuery, entityId)
	defer rows.Close()
	var id, validationStatus, nbView int64
	var creationDate, lastUpDate, step1Date, step2Date, step3Date time.Time
	var name, resume string
	var affairs []Affair
	for rows.Next() {
		err := rows.Scan(&id, &name, &resume, &validationStatus, &creationDate, &lastUpDate, &step1Date, &step2Date, &step3Date, &nbView)
		if err != nil {
			if err == sql.ErrNoRows {
				// no such affair id
			} else {
				panic(err)
			}
		} else {
			affairs = append(affairs, Affair{Id: id, Name: name, Resume: resume, ValidationStatus: validationStatus, CreationDate: creationDate, LastUpDate: lastUpDate, Step1Date: step1Date, Step2Date: step2Date, Step3Date: step3Date, NbView: nbView})
		}
	}
	return affairs
}

func (affair Affair) Save() Affair {
	if affair.Id == 0 {
		affair = affair.insert()
	} else {
		affair = affair.update()
	}
	return affair
}

func (affair Affair) insert() Affair {
	sqlQuery := "insert into AFFA.AFFA.AFFAIR(AFFA_NAME, AFFA_RESUME, AFFA_VALIDATION_STATUS, AFFA_START_DATE, AFFA_END_DATE, AFFA_CREATION_DATE, AFFA_LAST_UP_DATE, AFFA_STEP1_DATE, AFFA_STEP2_DATE, AFFA_STEP3_DATE) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)"
	affair.CreationDate = time.Now()
	affair.LastUpDate = time.Now()
	affair.Id = cnn.InsertQuery(sqlQuery, affair.Name, affair.Resume, affair.ValidationStatus, affair.StartDate, affair.EndDate, affair.CreationDate, affair.LastUpDate, affair.Step1Date, affair.Step2Date, affair.Step3Date)
	return affair
}

func (affair Affair) update() Affair {
	sqlQuery := "update AFFAIR set AFFA_NAME = ?, AFFA_RESUME = ?, AFFA_VALIDATION_STATUS = ?, AFFA_START_DATE = ?, AFFA_END_DATE = ?, AFFA_CREATION_DATE = ?, AFFA_LAST_UP_DATE = ?, AFFA_STEP1_DATE = ?, AFFA_STEP2_DATE = ?, AFFA_STEP3_DATE = ? where AFFA_ID = ?"
	affair.LastUpDate = time.Now()
	cnn.UpdateQuery(sqlQuery, affair.Name, affair.Resume, affair.ValidationStatus, affair.StartDate, affair.EndDate, affair.LastUpDate, affair.Step1Date, affair.Step2Date, affair.Step3Date, affair.Id)
	return affair
}
