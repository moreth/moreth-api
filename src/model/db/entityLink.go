package dbModel

import (
	"database/sql"

	cnn "gitlab.com/moreth/moreth-api/connection"
)

type EntityLink struct {
	Id             int64
	EntityParentId int64
	EntityChildId  int64
}

func FetchEntityLinkFromEntityLinkId(entityLinkId int64) EntityLink {
	sqlQuery := "select ENTI_PARENT_ID, ENTI_CHILD_ID from ENTITY_LINK where ENLI_ID = ?"
	rows := cnn.SelectQuery(sqlQuery, entityLinkId)
	defer rows.Close()
	var entityParentId, entityChildId int64
	var entityLink EntityLink
	err := rows.Scan(&entityParentId, &entityChildId)
	if err != nil {
		if err == sql.ErrNoRows {
			// no such entity link id
		} else {
			panic(err)
		}
	} else {
		entityLink = EntityLink{Id: entityLinkId, EntityParentId: entityParentId, EntityChildId: entityChildId}
	}
	return entityLink
}

func FetchEntityLinkFromEntityId(entityId int64) []AffairLink {
	var entityLinks []AffairLink
	for _, entityParent := range FetchEntityLinkFromEntityParentId(entityId) {
		entityLinks = append(entityLinks, entityParent)
	}
	for _, entityChild := range FetchEntityLinkFromEntityChildId(entityId) {
		entityLinks = append(entityLinks, entityChild)
	}
	return entityLinks
}

func FetchEntityLinkFromEntityParentId(entityParentId int64) []AffairLink {
	sqlQuery := "select ENLI_ID, ENTI_PARENT_ID, ENTI_CHILD_ID from ENTITY_LINK where ENLI_ID = ?"
	rows := cnn.SelectQuery(sqlQuery, entityParentId)
	defer rows.Close()
	var entityLinkId, entityChildId int64
	var entityLinks []AffairLink
	for rows.Next() {
		err := rows.Scan(&entityLinkId, &entityChildId)
		if err != nil {
			if err == sql.ErrNoRows {
				// no such entity link id
			} else {
				panic(err)
			}
		} else {
			entityLinks = append(entityLinks, AffairLink{Id: entityLinkId, AffairParentId: entityParentId, AffairChildId: entityChildId})
		}
	}
	return entityLinks
}

func FetchEntityLinkFromEntityChildId(entityChildId int64) []AffairLink {
	sqlQuery := "select ENLI_ID, ENTI_PARENT_ID, ENTI_CHILD_ID from ENTITY_LINK where ENLI_ID = ?"
	rows := cnn.SelectQuery(sqlQuery, entityChildId)
	defer rows.Close()
	var entityLinkId, entityParentId int64
	var entityLinks []AffairLink
	for rows.Next() {
		err := rows.Scan(&entityLinkId, &entityParentId)
		if err != nil {
			if err == sql.ErrNoRows {
				// no such entity link id
			} else {
				panic(err)
			}
		} else {
			entityLinks = append(entityLinks, AffairLink{Id: entityLinkId, AffairParentId: entityParentId, AffairChildId: entityChildId})
		}
	}
	return entityLinks
}

func (entityLink EntityLink) Save() EntityLink {
	if entityLink.Id == 0 {
		entityLink = entityLink.insert()
	} else {
		entityLink = entityLink.update()
	}
	return entityLink
}

func (entityLink EntityLink) insert() EntityLink {
	sqlQuery := "insert into ENTITY_LINK(ENTI_PARENT_ID, ENTI_CHILD_ID) VALUES (?, ?, ?, ?)"
	entityLink.Id = cnn.InsertQuery(sqlQuery, entityLink.EntityParentId, entityLink.EntityChildId)
	return entityLink
}

func (entityLink EntityLink) update() EntityLink {
	sqlQuery := "update ENTITY_LINK set ENTI_PARENT_ID = ?, ENTI_CHILD_ID = ? where ENLI_ID = ?"
	cnn.UpdateQuery(sqlQuery, entityLink.EntityParentId, entityLink.EntityChildId, entityLink.Id)
	return entityLink
}
