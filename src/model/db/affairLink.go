package dbModel

import (
	"database/sql"

	cnn "gitlab.com/moreth/moreth-api/connection"
)

type AffairLink struct {
	Id             int64
	AffairParentId int64
	AffairChildId  int64
}

func FetchAffairLinkFromAffairLinkId(affairLinkId int64) AffairLink {
	sqlQuery := "select AFFA_PARENT_ID, AFFA_CHILD_ID from AFFAIR_LINK where AFLI_ID = ?"
	rows := cnn.SelectQuery(sqlQuery, affairLinkId)
	defer rows.Close()
	var affairParentId, affairChildId int64
	var affairLink AffairLink
	err := rows.Scan(&affairParentId, &affairChildId)
	if err != nil {
		if err == sql.ErrNoRows {
			// no such affair link id
		} else {
			panic(err)
		}
	} else {
		affairLink = AffairLink{Id: affairLinkId, AffairParentId: affairParentId, AffairChildId: affairChildId}
	}
	return affairLink
}

func FetchAffairLinkFromAffairId(affairId int64) []AffairLink {
	var affairLinks []AffairLink
	for _, affairParent := range FetchAffairLinkFromAffairParentId(affairId) {
		affairLinks = append(affairLinks, affairParent)
	}
	for _, affairChild := range FetchAffairLinkFromAffairChildId(affairId) {
		affairLinks = append(affairLinks, affairChild)
	}
	return affairLinks
}

func FetchAffairLinkFromAffairParentId(affairParentId int64) []AffairLink {
	sqlQuery := "select AFLI_ID, AFFA_CHILD_ID from AFFAIR_LINK where AFFA_PARENT_ID = ?"
	rows := cnn.SelectQuery(sqlQuery, affairParentId)
	defer rows.Close()
	var affairLinkId, affairChildId int64
	var affairLinks []AffairLink
	for rows.Next() {
		err := rows.Scan(&affairLinkId, &affairChildId)
		if err != nil {
			if err == sql.ErrNoRows {
				// no such affair link id
			} else {
				panic(err)
			}
		} else {
			affairLinks = append(affairLinks, AffairLink{Id: affairLinkId, AffairParentId: affairParentId, AffairChildId: affairChildId})
		}
	}
	return affairLinks
}

func FetchAffairLinkFromAffairChildId(affairChildId int64) []AffairLink {
	sqlQuery := "select AFLI_ID, AFFA_PARENT_ID from AFFAIR_LINK where AFFA_CHILD_ID = ?"
	rows := cnn.SelectQuery(sqlQuery, affairChildId)
	defer rows.Close()
	var affairLinkId, affairParentId int64
	var affairLinks []AffairLink
	for rows.Next() {
		err := rows.Scan(&affairLinkId, &affairParentId)
		if err != nil {
			if err == sql.ErrNoRows {
				// no such affair link id
			} else {
				panic(err)
			}
		} else {
			affairLinks = append(affairLinks, AffairLink{Id: affairLinkId, AffairParentId: affairParentId, AffairChildId: affairChildId})
		}
	}
	return affairLinks
}

func (affairLink AffairLink) Save() AffairLink {
	if affairLink.Id == 0 {
		affairLink = affairLink.insert()
	} else {
		affairLink = affairLink.update()
	}
	return affairLink
}

func (affairLink AffairLink) insert() AffairLink {
	sqlQuery := "insert into AFFAIR_LINK(AFFA_PARENT_ID, AFFA_CHILD_ID) VALUES (?, ?, ?, ?)"
	affairLink.Id = cnn.InsertQuery(sqlQuery, affairLink.AffairParentId, affairLink.AffairChildId)
	return affairLink
}

func (affairLink AffairLink) update() AffairLink {
	sqlQuery := "update AFFAIR_LINK set AFFA_PARENT_ID = ?, AFFA_CHILD_ID = ? where AFLI_ID = ?"
	cnn.UpdateQuery(sqlQuery, affairLink.AffairParentId, affairLink.AffairChildId, affairLink.Id)
	return affairLink
}
