package main

import (
	"fmt"
	"log"
	"net/http"
	"time"

	httpConfiguration "gitlab.com/moreth/moreth-api/configuration/http"
	"gitlab.com/moreth/moreth-api/handler"
)

func main() {
	//http.HandleFunc("/api/affair", handler.Affair)
	http.HandleFunc("/api/search", handler.Search)
	http.HandleFunc("/api/value", handler.InfoValue)
	http.HandleFunc("/api/infoGroup", handler.InfoGroup)
	// var criteria1 model.Criteria = model.Criteria{
	// 	GroupName: "Personne physique",
	// 	Name:      "Nom",
	// 	Value:     "Édouard Philippe",
	// }
	// var criteria2 model.Criteria = model.Criteria{
	// 	GroupName: "Ecole",
	// 	Name:      "Nom",
	// 	Value:     "Institut",
	// }
	// for _, Search := range jsonModel.FetchSearchFromCriterias(criteria1, criteria2) {
	// 	json, _ := Search.ToJson()
	// 	fmt.Println(string(json))
	// }
	var s = &http.Server{
		Addr:           httpConfiguration.Host + ":" + fmt.Sprint(httpConfiguration.Port),
		ReadTimeout:    10 * time.Second,
		WriteTimeout:   10 * time.Second,
		MaxHeaderBytes: 1 << 20,
	}
	log.Fatal(s.ListenAndServe())
}
